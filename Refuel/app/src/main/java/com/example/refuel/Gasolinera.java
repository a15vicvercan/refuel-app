package com.example.refuel;

import androidx.annotation.NonNull;

public class Gasolinera {

    private String provincia;
    private String municipio;
    private String localidad;
    private String codigo_postal;
    private String direccion;
    private double longitud;
    private double latitud;
    private String gasolina95;
    private String gasolina98;
    private String gasoleoA;
    private String nuevo_gasoleoA;
    private String rotulo;
    private String horario;
    private String dia;

    //Constructores
    public Gasolinera () {

    }

    public Gasolinera (String provincia, String municipio, String localidad, String codigo_postal, String direccion, double longitud, double latitud, String gasolina95, String gasolina98, String gasoleoA, String nuevo_gasoleoA, String routulo, String horario, String dia) {

        this.provincia = provincia;
        this.municipio = municipio;
        this.localidad = localidad;
        this.codigo_postal = codigo_postal;
        this.direccion = direccion;
        this.longitud = longitud;
        this.latitud = latitud;
        this.gasolina95 = gasolina95;
        this.gasolina98 = gasolina98;
        this.gasoleoA = gasoleoA;
        this.nuevo_gasoleoA = nuevo_gasoleoA;
        this.rotulo = routulo;
        this.horario = horario;
        this.dia = dia;

    }


    //Getters
    public String getProvincia() {
        return provincia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public String getDireccion() {
        return direccion;
    }

    public double getLongitud() {
        return longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public String getGasolina95() {
        return gasolina95;
    }

    public String getGasolina98() {
        return gasolina98;
    }

    public String getGasoleoA() {
        return gasoleoA;
    }

    public String getNuevo_gasoleoA() {
        return nuevo_gasoleoA;
    }

    public String getRotulo() {
        return rotulo;
    }

    public String getHorario() {
        return horario;
    }

    public String getDia() {
        return dia;
    }


    //Setters
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public void setGasolina95(String gasolina95) {
        this.gasolina95 = gasolina95;
    }

    public void setGasolina98(String gasolina98) {
        this.gasolina98 = gasolina98;
    }

    public void setGasoleoA(String gasoleoA) {
        this.gasoleoA = gasoleoA;
    }

    public void setNuevo_gasoleoA(String nuevo_gasoleoA) {
        this.nuevo_gasoleoA = nuevo_gasoleoA;
    }

    public void setRotulo(String rotulo) {
        this.rotulo = rotulo;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }


    //toString
    @NonNull
    @Override
    public String toString() {
        return super.toString();
    }
}
