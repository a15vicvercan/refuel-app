package com.example.refuel;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.MapboxNavigation;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    //Variables de acceso a la BD en Firebase
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("gasolineras");

    //Variables del mapa y el generador de ruta hacia la gasolinera
    private MapView mapView;
    private MapboxMap mbMap;
    private PermissionsManager permissionsManager;
    private String accessToken = "pk.eyJ1IjoiYTE1dmljdmVyY2FuIiwiYSI6ImNrNWxlbDlpcTBtMTAzZW80Z2ducGpvZmUifQ.__GV1__ex9s_KuHz0BWlAg";
    private DirectionsRoute currentRoute;
    private NavigationMapRoute navigationMapRoute;
    private Point origin;
    private Point destination;

    //Variable global que representa una gasolinera
    private Gasolinera gasolinera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(this, accessToken);
        MapboxNavigation navigation = new MapboxNavigation(this, accessToken);

        setContentView(R.layout.activity_main);

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        /**
         * Diferenciamos entre la primera lectura u otra lectura que no sea la primera.
         * En caso de que sea la primera lectura de datos del día, descargamos los datos de la api.
         * En caso contrario, leemos directamente de la BD
         */
        lecturaDatosBD(new MyCallBack() {
            @Override
            public void onCallBack(ArrayList<Gasolinera> value) {

                Calendar fecha = Calendar.getInstance();
                String dia = String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
                ArrayList<Gasolinera> gasolineras = value;
                Gasolinera gasolinera = gasolineras.get(0);

                if(gasolinera.getDia().equals(dia)) {
                    Log.d("BBDD", "Lee directamente de firebase");
                    crearMapa();
                }
                else {
                    Log.d("BBDD", "Primero descarga los datos de la API");
                    new Descarregador().execute("https://www.mapabase.es/arcgis/rest/services/Otros/Gasolineras/FeatureServer/0/query?where=1%3D1&outFields=objectid,provincia,municipio,localidad,código_postal,dirección,longitud,latitud,precio_gasolina_95,precio_gasóleo_a,precio_nuevo_gasóleo_a,precio_gasolina_98,rótulo,horario&outSR=4326&f=json");
                }
            }
        });
    }

    /**
     * Función que crea el menú con las opciones de Tabla de precios y de Información
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    /**
     * Función que gestiona las acciones a realizar en cada opción del menú
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tabla_precios:
                Intent intentTabla = new Intent(this, TablaPrecios.class);
                startActivity(intentTabla);
                return true;
            case R.id.ajustes:
                Intent intentInformacion = new Intent(this, Informacio.class);
                startActivity(intentInformacion);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * Función que crea el mapa y le da el estilo necesario.
     * Función que es ejecutada de manera asíncnrona
     */
    public void crearMapa() {

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull final MapboxMap mapboxMap) {

                mbMap = mapboxMap;
                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull final Style style) {
                        // Map is set up and the style has loaded. Now you can add data or make other map adjustments.
                        enableLocationComponent(style);
                        initMapStuff(style);

                        mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(@NonNull Marker marker) {

                                //Mostrar informaciñon de la gasolinera marcada
                                mapboxMap.selectMarker(marker);

                                //Ruta de la ubicación actial a unas coordenadas concretas
                                origin = Point.fromLngLat(mbMap.getLocationComponent().getLastKnownLocation().getLongitude(), mbMap.getLocationComponent().getLastKnownLocation().getLatitude());
                                destination = Point.fromLngLat(marker.getPosition().getLongitude(), marker.getPosition().getLatitude());

                                NavigationRoute.builder(MainActivity.this)
                                        .accessToken(accessToken)
                                        .origin(origin)
                                        .destination(destination)
                                        .build()
                                        .getRoute(new Callback<DirectionsResponse>() {
                                            @Override
                                            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                                                currentRoute = response.body().routes().get(0);

                                                //Si hay una ruta mostrandose en el mapa, la elimina y genera la nueva y la muestra.
                                                // En caso contrario, la genera y la muestra.
                                                if (navigationMapRoute != null) {
                                                    navigationMapRoute.removeRoute();
                                                } else {
                                                    //Genera la ruta y la guarda en una variable
                                                    navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
                                                }
                                                //Muestra la ruta en el mapa
                                                navigationMapRoute.addRoute(currentRoute);

                                                //Obtenemos la distancia de la ruta, la pasamos a KM y la mostramos en un toast
                                                Double distanciaMetros = currentRoute.distance();
                                                Double distanciaKm = distanciaMetros/1000;
                                                String distanciaString = String.format("%.1f", distanciaKm);

                                                //Obtenemos la duracion de la ruta, la pasamos a minutos y la mostramos en un toast
                                                Double tiempoSegundos = currentRoute.duration();
                                                Double tiempoMinutos = (tiempoSegundos/60);
                                                String tiempo = String.format("%.0f", tiempoMinutos);

                                                String textoToast = "La distancia es de " + distanciaString + " KM." + "\nTiempo aproximado: " + tiempo + " minutos.";
                                                Toast toast = Toast.makeText(getApplicationContext(), textoToast, Toast.LENGTH_LONG);
                                                toast.show();
                                            }

                                            @Override
                                            public void onFailure(Call<DirectionsResponse> call, Throwable t) {

                                            }
                                        });
                                return true;
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * Función que inserta los marcadores en el mapa.
     * Estos se marcan basandose en la longitud y la latitud de la gasolinera.
     * @param style
     */
    public void initMapStuff(Style style) {

        //Obtenemos los datos de la BD
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        gasolinera = postSnapshot.getValue(Gasolinera.class);

                        //Consultamos los datos del valor de cada tipo de gasolina, en caso de ser null los marcamos con un guión (-)
                        String valorGas95 = "-";
                        String valorGas98 = "-";
                        String valorDies = "-";
                        String valorDiesA = "-";

                        //Si obtenemos que uno de los valores es diferente de null, guardamos el precio en la variable correspondiente
                        if (!(gasolinera.getGasolina95()).equals("null")) valorGas95 = gasolinera.getGasolina95();
                        if (!(gasolinera.getGasolina98()).equals("null")) valorGas98 = gasolinera.getGasolina98();
                        if (!(gasolinera.getGasoleoA()).equals("null")) valorDies = gasolinera.getGasoleoA();
                        if (!(gasolinera.getNuevo_gasoleoA()).equals("null")) valorDiesA = gasolinera.getNuevo_gasoleoA();

                        //En nuestra aplicación se diferencia el tipo de horario de cada gasolinera.
                        //Si el horario de la gasolinera es de 24H, se muestra en el mapa con un marcador verde.
                        //En caso de que no sea de 24h, se muestra con un marcador rojo.
                        //En ambos casos, cargamos los datos de cada gasolinera (Rotulo, horario, etc.)

                        //Caso de la gasolinera con horario 24h
                        if(gasolinera.getHorario().equals("L-D: 24H")) {
                            mbMap.addMarker(new MarkerOptions().position(new LatLng(gasolinera.getLatitud(), gasolinera.getLongitud()))
                                    .icon(IconFactory.getInstance(getApplicationContext()).fromResource(R.drawable.marcador_gasolinera_verde))
                                    .title(gasolinera.getRotulo())
                                    .snippet("Horario " + gasolinera.getHorario() +
                                            "\n" + "------------------------------" +
                                            "\n" + "Gasolina 95: " +  valorGas95 +
                                            "\n" + "Gasolina 98: " + valorGas98 +
                                            "\n" + "Diesel: " + valorDies +
                                            "\n" + "Diesel A: " + valorDiesA)
                            );
                        }
                        //Caso de la gasolinera con otro horario
                        else {
                            mbMap.addMarker(new MarkerOptions().position(new LatLng(gasolinera.getLatitud(), gasolinera.getLongitud()))
                                    .icon(IconFactory.getInstance(getApplicationContext()).fromResource(R.drawable.marcador_gasolinera_rojo))
                                    .title(gasolinera.getRotulo())
                                    .snippet("Horario " + gasolinera.getHorario() +
                                            "\n" + "------------------------------" +
                                            "\n" + "Gasolina 95: " +  valorGas95 +
                                            "\n" + "Gasolina 98: " + valorGas98 +
                                            "\n" + "Diesel: " + valorDies +
                                            "\n" + "Diesel A: " + valorDiesA)
                            );
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Lectura", "Failed to read value.", error.toException());
            }
        });
    }

    /**
     * Funciónn que pide permiso la localización cuando la app carga por primera vez.
     * Si se da permiso, la ubicación actual del usuario se mostrará en el mapa.
     * @param loadedMapStyle
     */
    private void enableLocationComponent(@NonNull final Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            LocationComponent locationComponent = mbMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);

        }
        else {
            permissionsManager = new PermissionsManager(new PermissionsListener() {
                @Override
                public void onExplanationNeeded(List<String> permissionsToExplain) {
                    Toast.makeText(getApplicationContext(), "location not enabled", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onPermissionResult(boolean granted) {
                    if (granted) {
                        mbMap.getStyle(new Style.OnStyleLoaded() {
                            @Override
                            public void onStyleLoaded(@NonNull Style style) {

                                LocationComponent locationComponent = mbMap.getLocationComponent();
                                locationComponent.activateLocationComponent(getApplicationContext(), loadedMapStyle);
                                locationComponent.setLocationComponentEnabled(true);
                                locationComponent.setCameraMode(CameraMode.TRACKING);
                                locationComponent.setRenderMode(RenderMode.COMPASS);
                            }
                        });
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Els serveis de localitzacio no estan activats", Toast.LENGTH_LONG).show();
                    }
                }
            });
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Función que lee los datos de la BD de Firebase
     * @param myCallBack
     */
    public void lecturaDatosBD(final MyCallBack myCallBack) {
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<Gasolinera>> t = new GenericTypeIndicator<ArrayList<Gasolinera>>() {};
                ArrayList<Gasolinera> value = dataSnapshot.getValue(t);

                myCallBack.onCallBack(value);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("ERROR", "Failed to read value.", error.toException());
            }
        });
    }

    /**
     * Clase, AsyncTask, que nos permite descargar los datos de la api web, parsearlos y guardarlos en la BD de la manera más conveniente
     */
    class Descarregador extends AsyncTask<String, Void, JSONArray> {

        @Override
        protected JSONArray doInBackground(String... strings) {
            JSONArray bloquesGasolineras = null;

            try {
                URL url = new URL(strings[0]);
                HttpURLConnection client = (HttpURLConnection) url.openConnection();
                client.connect();

                InputStream is = client.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(isr);
                String strCurrentLine = null;

                while ((strCurrentLine = reader.readLine()) != null) {
                    JSONObject json = new JSONObject(strCurrentLine);
                    bloquesGasolineras = json.getJSONArray("features");
                }
                client.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (final JSONException e) {
                e.printStackTrace();
            }
            return bloquesGasolineras;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(JSONArray bloquesGasolineras) {
            super.onPostExecute(bloquesGasolineras);

            Calendar fecha = Calendar.getInstance();

            ArrayList<Gasolinera> arrayGasolineras = new ArrayList<Gasolinera>();
            JSONObject subBloque = null;
            JSONObject gasolinera = null;
            Gasolinera gasolinera1 = null;

            String provincia = null;
            String municipio = null;
            String localidad = null;
            String codigo_postal = null;
            String direccion = null;
            double longitud;
            double latitud;
            String gasolina95 = null;
            String gasolina98 = null;
            String gasoleoA = null;
            String nuevo_gasoleoA = null;
            String rotulo = null;
            String horario = null;
            String dia = String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
            int longArray = bloquesGasolineras.length();

            for(int i = 0; i < longArray; i++) {
                try {
                    subBloque = bloquesGasolineras.getJSONObject(i);
                    gasolinera = subBloque.getJSONObject("attributes");

                    provincia = gasolinera.getString("provincia");
                    municipio = gasolinera.getString("municipio");
                    localidad = gasolinera.getString("localidad");
                    codigo_postal = gasolinera.getString("código_postal");
                    direccion = gasolinera.getString("dirección");
                    longitud = gasolinera.getDouble("longitud");
                    latitud = gasolinera.getDouble("latitud");
                    if (!gasolinera.getString("precio_gasolina_95").equals(null)) {
                        gasolina95 = gasolinera.getString("precio_gasolina_95");
                    }
                    if (!gasolinera.getString("precio_gasolina_98").equals(null)) {
                        gasolina98 = gasolinera.getString("precio_gasolina_98");
                    }
                    if (!gasolinera.getString("precio_gasóleo_a").equals(null)) {
                        gasoleoA = gasolinera.getString("precio_gasóleo_a");
                    }
                    if (!gasolinera.getString("precio_nuevo_gasóleo_a").equals(null)) {
                        nuevo_gasoleoA = gasolinera.getString("precio_nuevo_gasóleo_a");
                    }
                    rotulo = gasolinera.getString("rótulo");
                    horario = gasolinera.getString("horario");

                    gasolinera1 = new Gasolinera(provincia, municipio, localidad, codigo_postal, direccion, longitud, latitud, gasolina95, gasolina98, gasoleoA, nuevo_gasoleoA, rotulo, horario, dia);
                    arrayGasolineras.add(gasolinera1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            myRef.setValue(arrayGasolineras);
            crearMapa();
        }
    }
}