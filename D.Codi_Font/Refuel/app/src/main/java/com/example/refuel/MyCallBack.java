package com.example.refuel;

import java.util.ArrayList;

public interface MyCallBack {
    void onCallBack(ArrayList<Gasolinera> value);
}
