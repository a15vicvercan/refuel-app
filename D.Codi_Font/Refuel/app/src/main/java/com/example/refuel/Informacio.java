package com.example.refuel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Informacio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacio);
    }

    /**
     * Función que abre la app de Gmail para enviar un correo
     * @param view
     */
    public void onClickOpenGmail(View view) {
        //Obtenemos el mail que pone en el textview
        TextView emailTxtView = findViewById(R.id.txtViewMailContent);
        String emailTo = emailTxtView.getText().toString();

        //Creamos el intent con los parametros que queremos que tenga el mail
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {emailTo});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Solicito información o incidencia");
        intent.putExtra(Intent.EXTRA_TEXT, "Este es el cuerpo del mensaje");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    /**
     * Función que abre la app de telefono para llamar por telefono
     * @param view
     */
    public void onClickLlamarTlf(View view) {
        //Diferenciamos que numero es, dependiendo de que textview elige el usuario
        TextView numeroTlf1 = findViewById(R.id.txtViewTlf1);
        int id = numeroTlf1.getId();

        TextView numeroTlf2 = findViewById(R.id.txtViewTlf2);
        int id2 = numeroTlf2.getId();

        String phoneNumber = "";

        if (view.getId() == id) {
            phoneNumber = "633787436";
        }
        else if (view.getId() == id2) {
            phoneNumber = "673110152";
        }

        //Creamos el intent con el numemro elegido
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
