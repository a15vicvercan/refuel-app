package com.example.refuel;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TablaPrecios extends AppCompatActivity {

    //Variables de acceso a la BD en Firebase
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("gasolineras");

    //Variables del RecyclerView
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabla_precios);

        lecturaDatosBD(new MyCallBack() {
            @Override
            public void onCallBack(ArrayList<Gasolinera> value) {

                RecyclerView recyclerView = findViewById(R.id.rvTablaPrecios);
                recyclerView.setHasFixedSize(true);

                layoutManager = new LinearLayoutManager(TablaPrecios.this);
                recyclerView.setLayoutManager(layoutManager);

                mAdapter = new GasolineraAdapter(value, "-");
                recyclerView.setAdapter(mAdapter);
            }
        });
    }

    /**
     * Función que lee los datos de la BD de Firebase
     * @param myCallBack
     */
    public void lecturaDatosBD(final MyCallBack myCallBack) {
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<Gasolinera>> t = new GenericTypeIndicator<ArrayList<Gasolinera>>() {};
                ArrayList<Gasolinera> value = dataSnapshot.getValue(t);

                myCallBack.onCallBack(value);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("ERROR", "Failed to read value.", error.toException());
            }
        });
    }

    /**
     * Función que carga el listado de las gasolineras con el municipio que introduce el usuario
     * @param view
     */
    public void onClickBuscar(View view) {

        //Obtenemos el municipio que introduce el usuario y lo pasamos a mayusculas para buscarlo en la BD
        EditText editText = findViewById(R.id.eTxtMunicipio);
        String municipio = editText.getText().toString();
        final String municipioMayusculas = municipio.toUpperCase();

        //Buscamos las gasolineras del municipio que pide el usuario
        lecturaDatosBD(new MyCallBack() {
            @Override
            public void onCallBack(ArrayList<Gasolinera> value) {
                ArrayList<Gasolinera> gasolinerasMunicipio = new ArrayList<>();

                for(int i = 0; i < value.size(); i++) {
                    Gasolinera gasolinera = value.get(i);

                    if(gasolinera.getMunicipio().contains(municipioMayusculas)) {
                        gasolinerasMunicipio.add(gasolinera);
                    }
                }

                //Cargamos los datos en el RecyclerView

                RecyclerView recyclerView = findViewById(R.id.rvTablaPrecios);
                recyclerView.setHasFixedSize(true);

                layoutManager = new LinearLayoutManager(TablaPrecios.this);
                recyclerView.setLayoutManager(layoutManager);

                mAdapter = new GasolineraAdapter(gasolinerasMunicipio, municipioMayusculas);
                recyclerView.setAdapter(mAdapter);
            }
        });
    }
}
