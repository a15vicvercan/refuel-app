package com.example.refuel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class GasolineraAdapter extends RecyclerView.Adapter<GasolineraAdapter.GasolineraViewHolder> {
    //Atributos que contendrá los datos necesarios para cargarlos en el RecylcerView
    private ArrayList<Gasolinera> listadoGasolineras;
    private String municipio;

    public GasolineraAdapter(ArrayList<Gasolinera> data, String municipio) {
        this.listadoGasolineras = data;
        this.municipio = municipio;
    }

    @NonNull
    @Override
    public GasolineraViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bloc, parent, false);
        return new GasolineraViewHolder(row);
    }

    /**
     * Función que cargará los datos en el RecyclerView
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(GasolineraViewHolder holder, int position) {
        Gasolinera gasolinera = this.listadoGasolineras.get(position);

        //Consultamos los datos del valor de cada tipo de gasolina y si tiene dirección o no en la BD, en caso de ser null los marcamos con un guión (-)
        String direccion = "-";
        String precio95 = "-";
        String precio98 = "-";
        String precioDiesel = "-";
        String precioDieselA = "-";

        String marcaGasolinera = gasolinera.getRotulo();
        String horarioGasolinera = gasolinera.getHorario();

        //Si obtenemos que uno de los valores es diferente de null, guardamos el precio en la variable correspondiente
        if (!gasolinera.getDireccion().equals(("null"))) direccion = gasolinera.getDireccion();
        if (!gasolinera.getGasolina95().equals(("null"))) precio95 = gasolinera.getGasolina95() + " €";
        if (!gasolinera.getGasolina98().equals(("null"))) precio98 = gasolinera.getGasolina98() + " €";
        if (!gasolinera.getGasoleoA().equals(("null"))) precioDiesel = gasolinera.getGasoleoA() + " €";
        if (!gasolinera.getNuevo_gasoleoA().equals(("null"))) precioDieselA = gasolinera.getNuevo_gasoleoA() + " €";

        //Insertamos los datos para que se muestren por pantalla
        holder.getTxtView_Marca().setText(gasolinera.getRotulo());
        holder.getTxtView_horario().setText("Horario: " + horarioGasolinera);
        holder.getTxtView_direccion().setText(direccion + " (" + gasolinera.getMunicipio() + ")");
        holder.getTxtView_precio95().setText("Gasolina 95: " + precio95);
        holder.getTxtView_precio98().setText("Gasolina 98: " + precio98);
        holder.getTxtView_precioDiesel().setText("Diesel: " + precioDiesel);
        holder.getTxtView_precioDieselA().setText("Diesel A: " + precioDieselA);

        //Según lo que obtengamos del campo rótulo, indicaremos con una imagen su valor
        switch (marcaGasolinera) {
            case "CEPSA":
                holder.getImg_logo().setImageResource(R.drawable.logo_cepsa);
                break;
            case "REPSOL":
                holder.getImg_logo().setImageResource(R.drawable.logo_repsol);
                break;
            case "ESCLATOIL":
                holder.getImg_logo().setImageResource(R.drawable.logo_esclat);
                break;
            case "SHELL":
                holder.getImg_logo().setImageResource(R.drawable.logo_shell);
                break;
            case "GALP":
                holder.getImg_logo().setImageResource(R.drawable.logo_galp);
                break;
            case "EROSKI":
                holder.getImg_logo().setImageResource(R.drawable.logo_eroski);
                break;
            case "PETRONOR":
                holder.getImg_logo().setImageResource(R.drawable.logo_petronor);
                break;
            case "BP":
                holder.getImg_logo().setImageResource(R.drawable.logo_bp);
                break;
            case "CARREFOUR":
                holder.getImg_logo().setImageResource(R.drawable.logo_carrefour);
                break;
            case "ALCAMPO":
                holder.getImg_logo().setImageResource(R.drawable.logo_alcampo);
                break;
            default:
                holder.getImg_logo().setImageResource(R.drawable.icono_corto);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return this.listadoGasolineras.size();
    }

    /**
     * Clase que hace referencia a cada elemento que se muestra en el RecyclerView
     */
    public class GasolineraViewHolder extends RecyclerView.ViewHolder {

        //Atributos que hacen referencia a los elementos de cada bloque del RecyclerView
        private TextView txtView_Marca;
        private TextView txtView_direccion;
        private TextView txtView_horario;
        private TextView txtView_precio95;
        private TextView txtView_precio98;
        private TextView txtView_precioDiesel;
        private TextView txtView_precioDieselA;
        private ImageView img_logo;

        /**
         * Creadora de la clase
         * @param itemView
         */
        public GasolineraViewHolder(View itemView) {
            super(itemView);
            this.txtView_Marca = itemView.findViewById(R.id.txtMarcaGasolinera);
            this.txtView_direccion = itemView.findViewById(R.id.txtDireccionGasolinera);
            this.txtView_horario = itemView.findViewById(R.id.txtHorarioGasolinera);
            this.txtView_precio95 = itemView.findViewById(R.id.txtPrecio95);
            this.txtView_precio98 = itemView.findViewById(R.id.txtPrecio98);
            this.txtView_precioDiesel = itemView.findViewById(R.id.txtPrecioDiesel);
            this.txtView_precioDieselA = itemView.findViewById(R.id.txtPrecioDieselA);
            this.img_logo = itemView.findViewById(R.id.imgViewMarca);
        }

        /**
         * Getters de los elementos del RecyclerView
         */

        public TextView getTxtView_Marca() {
            return txtView_Marca;
        }

        public TextView getTxtView_direccion() {
            return txtView_direccion;
        }

        public TextView getTxtView_precio95() {
            return txtView_precio95;
        }

        public TextView getTxtView_precio98() {
            return txtView_precio98;
        }

        public TextView getTxtView_precioDiesel() {
            return txtView_precioDiesel;
        }

        public TextView getTxtView_precioDieselA() {
            return txtView_precioDieselA;
        }

        public ImageView getImg_logo() {
            return img_logo;
        }

        public TextView getTxtView_horario() {
            return txtView_horario;
        }
    }

    public void add(int position, Gasolinera item) {
        this.listadoGasolineras.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        this.listadoGasolineras.remove(position);
        notifyItemRemoved(position);
    }
}
