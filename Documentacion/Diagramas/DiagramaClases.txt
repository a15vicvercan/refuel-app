gasolinera {
    Fecha: {
        type: String,
    },
    "Codigo Postal": {
        type: String,
    },
    Dirección: {
        type: String,
    },
    Horario: {
        type: String,
    },
    Latitud: {
        type: String,
    },
    Localidad: {
        type: String,
    },
    "Longitud (WGS84)": {
        type: String,
    },
    Municipio: {
        type: String,
    },
    "Precio Biodiesel": {
        type: String,
    },
    "Precio Bioetanol": {
        type: String,
    },
    "Precio Gas Natural Comprimido": {
        type: String,
    },
    "Precio Gas Natural Licuado": {
        type: String,
    },
    "Precio Gases licuados del petróleo": {
        type: String,
    },
    "Precio Gasoleo A": {
        type: String,
    },
    "Precio Gasoleo B": {
        type: String,
    },
    "Precio Gasolina 95 Protección": {
        type: String,
    },
    "Precio Gasolina 98": {
        type: String,
    },
    "Precio Nuevo Gasoleo A": {
        type: String,
    },
    Provincia: {
        type: String,
    },
    Rótulo: {
        type: String,
    },
    IDEESS: {
        type: String,
    },
    IDMunicipio: {
        type: String,
    },
    IDProvincia: {
        type: String,
    },
    IDCCAA: {
        type: String,
    }
}